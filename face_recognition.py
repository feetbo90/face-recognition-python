from PIL import Image
from numpy import asarray
from mtcnn.mtcnn import MTCNN
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

image = Image.open('steve.jpeg')
# convert to RGB, if needed
image = image.convert('RGB')
# convert to array
pixels = asarray(image)
detector = MTCNN()

results = detector.detect_faces(pixels)
x1, y1, width, height = results[0]['box']
# x1 = 2
print(x1)